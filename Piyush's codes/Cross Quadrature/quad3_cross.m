function [X, W] = quad3_cross(Xqud, Wqud)


        n1 = Xqud(:, 1);
        n2 = Xqud(:, 2);
        n3 = Xqud(:, 3);
        n4 = Xqud(:, 4);
        z  = Xqud(:, 5);
        
        
        oneColumn = 0*z+1;
%         
%         W1 = Wqud .* z.^4 .* n1 .* n3;
%         X1 = z .* [n3,...
%             n3.*n4,...
%             oneColumn,...
%             n1.*n2,...
%             n1.*(1-n2)];
%         
%         W2 = Wqud .* z.^4 .* n2.^2 .* n3;
%         X2 = z .* [oneColumn,...
%             n1,...
%             n2,...
%             n2.*n3.*n4,...
%             n2.*n3.*(1-n4)];


        W1 = Wqud .* z.^4 .* n1 .* n3;
        X1 = z .* [n3.*n4,...
            n3.*(1-n4),...
            n1.*n2,...
            n1.*(1-n2),...
            (1-n1)];
        
        W2 = Wqud .* z.^4 .* n2.^2 .* n3;
        X2 = z .* [n1,...
            (1-n1),...
            n2.*n3.*n4,...
            n2.*n3.*(1-n4),...
            n2.*(1-n3)];
        W = [W1;W2];
        X = [X1;X2];


end