function out = divcurlfreeM1(X)
    out = [X(:,1) -2*X(:,2) X(:,3)];
end