function out = rotConstM(X)
    out = ones(size(X,1),1) * [1 2 3];
end