function out = constM(X)
    out = ones(size(X,1),1) * [1 0 0];
end