function M = mass_matrix(Gamma,test,trial)
    M = integral(Gamma,test,trial);
end